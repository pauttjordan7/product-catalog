import cors from 'cors';
import apiRouter from './api/routes';
import express, { NextFunction, Request, Response } from 'express';
import { responseHandler } from './lib/middlewares/response.middleware';

const app: express.Application = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((_req: Request, res: Response, next: NextFunction) => {
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE',
  );
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  return next();
});

app.use('/api', apiRouter);
app.use('/health-check', (_req, res) => res.sendStatus(200));

app.use('*', (_req, res) => res.sendStatus(404));

app.use(responseHandler);

export default app;
