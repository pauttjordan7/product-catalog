import dotenv from 'dotenv';
dotenv.config();

export default Object.freeze({
  env: process.env.NODE_ENV,
  port: process.env.PORT ?? '8080',
  DB: {
    URI: process.env.MONGODB_URI,
    USER: process.env.MONGODB_USER || 'admin',
    PASSWORD: process.env.MONGODB_PASSWORD || 'admin',
  },
});
