import mongoose, { ConnectOptions } from 'mongoose';
import config from './index';

const dbOptions: ConnectOptions = {
  autoIndex: true,
  user: config.DB.USER,
  pass: config.DB.PASSWORD,
};

mongoose.connect(config.DB.URI, dbOptions);

const connection = mongoose.connection;

connection.once('open', () => {
  console.log('Mongodb Connection stablished');
});

connection.on('error', (err) => {
  console.log('Mongodb connection error:', err);
  process.exit();
});
