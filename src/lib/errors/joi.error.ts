import ResponseError from '../models/response-error.model';

export declare type errorsGroup = Record<
  string,
  (...args: any) => ResponseError
>;

const joiErrors: errorsGroup = {
  unevaluatedFields(type: 'body' | 'query' | 'params' | 'headers') {
    return new ResponseError({
      code: `UNVALIDATED_${type.toUpperCase()}_CONTENT`,
      message: `Incoming <${type}> data in not being validated`,
      status: 400,
    });
  },

  invalidFields(error: string, type: string) {
    return new ResponseError({
      code: `INVALID_${type.toUpperCase()}_VALUE_CONTENT`,
      message: error,
      status: 400,
    });
  },
};

export default joiErrors;
