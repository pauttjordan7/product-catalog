export interface IResponse {
  status: string | number;
  message: string;
  data: any;
}
