import { ErrorRequestHandler } from 'express';
import ResponseError from '../models/response-error.model';
import ResponseSuccess from '../models/response-success.model';

export const responseHandler: ErrorRequestHandler = (
  response: ResponseError | ResponseSuccess,
  _req,
  res,
  next,
) => {
  if (response instanceof ResponseError) {
    return res.status(Number(response.status)).json({
      status: response.status,
      message: response.message,
      data: response.data,
    });
  } else if (response instanceof ResponseSuccess) {
    res.locals.message = response.message;
    return res.status(Number(response.status)).json({
      status: response.status,
      message: response.message,
      data: response.data,
    });
  }
  return next(response);
};
