import { Schema } from 'joi';
import { Response, Request, NextFunction } from 'express';
import joiErrors from '../errors/joi.error';

export type Mutable<T> = {
  -readonly [K in keyof T]: T[K];
};

export const validateSchema = (
  schemas: Partial<Record<keyof Request, Schema>>,
) => {
  const validationOptions = {
    abortEarly: false, // abort after the last validation error
    allowUnknown: true, // allow unknown keys that will be ignored
    stripUnknown: true, // remove unknown keys from the validated data
  };

  return (req: Mutable<Request>, _res: Response, next: NextFunction) => {
    let joiError: Error | null = null;

    Object.entries(schemas).every(([source, schema]) => {
      if (schema) {
        const { error, value } = schema.validate(
          getProperty(req, source as keyof Request),
          validationOptions,
        );
        if (error != null) {
          joiError = joiErrors.invalidFields(
            error.details.map(({ message }) => message).join('; '),
            source,
          );
          return false;
        }
        setProperty(req, source as keyof Request, value);
        return true;
      }
      return false;
    });

    if (joiError) {
      return next(joiError);
    }

    return next();
  };
};

function getProperty<T, K extends keyof T>(o: T, propertyName: K): T[K] {
  return o[propertyName];
}

function setProperty<T, K extends keyof T>(
  o: T,
  propertyName: K,
  value: any,
): any {
  o[propertyName] = value;
}
