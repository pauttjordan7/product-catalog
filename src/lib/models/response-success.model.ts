import { IResponse } from '../interfaces/response.interface';

export default class ResponseSuccess implements IResponse {
  status: string | number;
  message: string;
  data: any;

  constructor(message: string, data: any = null) {
    this.status = 200;
    this.message = message;
    this.data = data;
  }
}
