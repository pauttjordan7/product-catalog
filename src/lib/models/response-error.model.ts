import { IResponse } from '../interfaces/response.interface';

export default class ResponseError extends Error implements IResponse {
  status: string | number;
  message: string;
  data: any;

  constructor(error: any) {
    super(error?.message ?? 'Algo ha salido mal');
    this.parserError(error);
  }

  private parserError(error: any): void {
    this.status = error?.status ?? 500;
    this.message = error?.message ?? 'Algo ha salido mal';
    this.data = null;
  }
}
