import http from 'http';
import app from './app';
import config from './config';
import './config/database';

const port = parseInt(config.port, 10);
const server = http.createServer(app);

server.listen(port, '0.0.0.0', () => {
  console.log(`Server Running on Port ${port}`);
});
