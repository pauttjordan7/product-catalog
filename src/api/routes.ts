import { Router } from 'express';
import productRoutes from './product/product.routes';

const router: Router = Router();

router.use('/product', productRoutes);

export default router;
