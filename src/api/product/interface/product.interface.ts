export interface IProduct {
  _id: string;
  sku: string;
  name: string;
  description: string;
  image: string;
  label: string[];
  price: number;
  stock: number;
  history: any[];
}

export interface IProductUpdate
  extends Omit<IProduct, 'sku' | '_id' | 'history'> {}

export interface ISequenceSku {
  seq: number;
}

export interface IHistory {
  date: Date;
  price: number;
  stock: number;
  action: string;
}
