import { model, Schema } from 'mongoose';
import { IProduct } from '../interface/product.interface';

const HistorySchema = new Schema({
  date: {
    type: Date,
    default: Date.now,
  },
  price: {
    type: Number,
    required: true,
  },
  stock: {
    type: Number,
    required: true,
  },
  action: {
    type: String,
    required: true,
  },
});

const productSchema = new Schema({
  sku: {
    type: String,
    unique: true,
    required: true,
    index: true,
  },
  name: {
    type: String,
    unique: true,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  label: {
    type: [String],
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  stock: {
    type: Number,
    required: true,
  },
  history: [HistorySchema],
});

export default model<IProduct>('Product', productSchema);
