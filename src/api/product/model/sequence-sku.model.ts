import { model, Schema } from 'mongoose';
import { ISequenceSku } from '../interface/product.interface';

const SequenceSkuSchema = new Schema({
  _id: String,
  seq: { type: Number, default: 0 },
});

export default model<ISequenceSku>('SequenceSku', SequenceSkuSchema);
