import { RequestHandler } from 'express';
import { ProductService } from './product.service';
import ResponseSuccess from '../../lib/models/response-success.model';
import ResponseError from '../../lib/models/response-error.model';
import { IProduct } from './interface/product.interface';
import { ObjectId } from 'mongoose';

export class ProductController {
  private readonly productService = new ProductService();

  getAll: RequestHandler = async (req, res, next): Promise<any> => {
    try {
      const result = await this.productService.findAll();
      return next(new ResponseSuccess('Productos Encontrados', result));
    } catch (error) {
      return next(new ResponseError(error));
    }
  };

  getOne: RequestHandler = async (req, res, next): Promise<any> => {
    try {
      const productId: ObjectId = req.params.id as unknown as ObjectId;
      const result = await this.productService.findOne(productId);
      return next(new ResponseSuccess('Producto Encontrado', result));
    } catch (error) {
      return next(new ResponseError(error));
    }
  };

  create: RequestHandler = async (req, res, next): Promise<any> => {
    try {
      const productData: IProduct = req.body;
      const result = await this.productService.create(productData);
      return next(new ResponseSuccess('Producto Creado', result));
    } catch (error) {
      return next(new ResponseError(error));
    }
  };

  update: RequestHandler = async (req, res, next): Promise<any> => {
    try {
      const productData: IProduct = req.body;
      const productId: ObjectId = req.params.id as unknown as ObjectId;

      const result = await this.productService.update(productId, productData);
      return next(new ResponseSuccess('Producto Modificado', result));
    } catch (error) {
      return next(new ResponseError(error));
    }
  };

  remove: RequestHandler = async (req, res, next): Promise<any> => {
    try {
      const productId: ObjectId = req.params.id as unknown as ObjectId;
      const result = await this.productService.remove(productId);
      return next(new ResponseSuccess('Producto Eliminado', result));
    } catch (error) {
      return next(new ResponseError(error));
    }
  };
}
