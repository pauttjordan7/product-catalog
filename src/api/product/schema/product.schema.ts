import Joi from 'joi';

const objectIdSchema = Joi.string()
  .regex(/^[0-9a-fA-F]{24}$/)
  .required();

export const getOne = {
  params: Joi.object({
    id: objectIdSchema,
  }),
};

export const create = {
  body: Joi.object({
    name: Joi.string().min(2).required(),
    label: Joi.array().required(),
    price: Joi.number().required(),
    stock: Joi.number().required(),
    description: Joi.string().min(5).required(),
    image: Joi.string().uri().required(),
  }).messages({
    'any.required': '{#key} es requerido',
  }),
};

export const update = {
  params: Joi.object({
    id: objectIdSchema,
  }),

  body: Joi.object({
    name: Joi.string().min(2).optional(),
    label: Joi.array().optional(),
    price: Joi.number().optional(),
    stock: Joi.number().optional(),
    description: Joi.string().min(5).optional(),
    image: Joi.string().uri().optional(),
  }).messages({
    'any.required': '{#key} es requerido',
  }),
};

export const remove = {
  params: Joi.object({
    id: objectIdSchema,
  }),
};
