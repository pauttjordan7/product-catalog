import { Router } from 'express';
import { ProductController } from './product.controller';
import { validateSchema } from '../../lib/middlewares/schema-validate.middleware';
import { create, getOne, remove, update } from './schema/product.schema';

const app: Router = Router();
const productController = new ProductController();

app.get('/', productController.getAll);
app.post('/', validateSchema(create), productController.create);
app.get('/:id', validateSchema(getOne), productController.getOne);
app.put('/:id', validateSchema(update), productController.update);
app.delete('/:id', validateSchema(remove), productController.remove);

export default app;
