import { ObjectId } from 'mongoose';
import { IProduct, IProductUpdate } from './interface/product.interface';
import ProductModel from './model/product.model';
import SequenceSkuModel from './model/sequence-sku.model';

export class ProductService {
  async findAll(): Promise<IProduct[]> {
    return await ProductModel.find();
  }

  async findOne(id: ObjectId): Promise<IProduct> {
    return await ProductModel.findOne({ _id: id });
  }

  async create(productValue: IProduct): Promise<IProduct> {
    const sequence = await SequenceSkuModel.findByIdAndUpdate(
      { _id: 'productSKU' },
      { $inc: { seq: 1 } },
      { new: true, upsert: true },
    );
    const sequenceParser = `${sequence._id}${sequence.seq}`;

    const product = new ProductModel({ ...productValue, sku: sequenceParser });
    return await product.save();
  }

  async update(id: ObjectId, productValue: IProductUpdate): Promise<IProduct> {
    await ProductModel.updateOne({ _id: id }, { ...productValue });
    const productUpdate = await ProductModel.findOne({ _id: id });

    if (productValue.price || productValue.stock) {
      const historyEntry = {
        date: new Date(),
        price: productValue.price || productUpdate.price,
        stock: productValue.stock || productUpdate.stock,
        action: 'update',
      };
      productUpdate.history.push(historyEntry);
      await productUpdate.save();
    }

    return productUpdate;
  }

  async remove(id: ObjectId): Promise<IProduct> {
    return ProductModel.findByIdAndDelete({ _id: id });
  }
}
